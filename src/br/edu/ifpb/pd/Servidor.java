package br.edu.ifpb.pd;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Servidor {

	public static void main(String[] args) throws IOException {
		ServerSocket servidor = new ServerSocket(1234);
		Socket socket;
		DataInputStream in;
		DataOutputStream out;

		do {
			socket = servidor.accept();
			in = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());
			String filename = in.readUTF();

			try {
				File arquivo = new File("src/files/" + filename);
				String content = "200 - Arquivo nao encontrado.\n";

				Scanner sc = new Scanner(arquivo);
				while (sc.hasNextLine()) {
					content += sc.nextLine() + "\n";
				}
				out.writeUTF(content);

			} catch (FileNotFoundException ex) {
				out.writeUTF("400 - Arquivo nao encontrado.");
			}

		} while (servidor.getInetAddress() != null);
		in.close();
		socket.close();
	}

}
