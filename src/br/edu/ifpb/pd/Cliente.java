package br.edu.ifpb.pd;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {

	public static void main(String[] args) throws IOException {
		System.out.println("Informe URI: ");
		Scanner teclado = new Scanner(System.in);
		String uri = teclado.nextLine();

		while (!isValid(uri)) {
			System.out.println("URI informado invalido, tente novamente");
			uri = teclado.nextLine();
		}
														 // regex
		String host = uri.replaceAll("\\:.*", ""); 		 // pega o host
		String port = uri.replaceAll(".*\\:|\\/.*", ""); // pega o numero da porta
		String filename = uri.replaceAll(".*\\/", ""); 	 // pega o nome do arquivo

		Socket socket;

		try {
			socket = new Socket(host, Integer.parseInt(port));

			DataInputStream in = new DataInputStream(socket.getInputStream());
			DataOutputStream out = new DataOutputStream(
					socket.getOutputStream());

			out.writeUTF(filename);

			String content = in.readUTF();
			System.out.println(content);

			socket.close();

		} catch (Exception e) {
			System.out.println("Nao houve conexao");
		}
	}

	/* -------------- FUNCOES -------------- */

	// checa se opcao informada pelo cliente eh valida
	public static boolean isValid(String str) {
		if (str.contains(":") && str.contains("/"))
			return true;

		return false;
	}

}
