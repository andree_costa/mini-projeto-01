package web;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Pedido implements Runnable {
	
	private Socket client;
	
	public Pedido(Socket client) {
		this.client = client;
	}
	
	public void run() {
		
		Resposta r = new Resposta();
		String request;
		DataInputStream in;
		ObjectOutputStream out;
		
		try {
			in = new DataInputStream(client.getInputStream());
			out = new ObjectOutputStream(client.getOutputStream());
			
			request = in.readUTF();
			
			String dados = readFile(request);
			
			if (dados != null) {
				r.setStatus(200);
				r.setData(dados);
			}
			else
				r.setStatus(400);

			out.writeObject(r);
			
			client.close();
			
		} catch (IOException e) { //InterruptedException
			e.printStackTrace();
			return;
		}
	}
	
	private String readFile(String path) {
		String dados;
		
		if (path.contains("/") || path.contains("\\"))
			return null;
		
		try {
			dados = new String(Files.readAllBytes(Paths.get(path)));
		} catch (IOException e) {
			return null;
		}
		
		return dados;
	}
}
