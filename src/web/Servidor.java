package web;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Servidor {
	
	private ServerSocket ssocket;
	boolean running;
	
	//List<Pedido> request;
	
	public Servidor() {
	}
	
	private void waitClient() {
		Socket client;
		
		System.out.println("Aguardando por clientes.");
		
		while(ssocket.isBound()) {
			try {
				client = ssocket.accept();
				//Quando o servidor recebe uma requisi��o, delega � uma classe responsavel por tratar os pedidos
				Thread t = new Thread(new Pedido(client));
				t.start();
			}
			catch (SocketException e) {
				break;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void start(int porta) throws IOException {
		if (ssocket != null && !ssocket.isClosed())
			ssocket.close();
		
		ssocket = new ServerSocket(porta);
		
		if (!ssocket.isClosed())
			System.out.println("Servidor iniciado.");
		else
			System.out.println("Falha ao iniciar o servidor.");
		
		new Thread(new Runnable(){
	        public void run() {
	        	waitClient();
	        }
	    }).start();
	}
	
	public void stop() throws IOException {
		if (ssocket == null || ssocket.isClosed()) {
			System.out.println("O servidor ainda n�o foi iniciado.");
			return;
		}
		
		ssocket.close();
		System.out.println("Servidor interrompido.");
	}
	
	public boolean isRunning() {
		return !ssocket.isClosed();
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		Servidor s = new Servidor();
		
		s.start(4445);

		while(s.isRunning()) {
			Thread.sleep(1);
		}
	}
}
