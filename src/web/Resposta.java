package web;

import java.io.Serializable;

public class Resposta implements Serializable {
	
	private static final long serialVersionUID = -8106451555358854981L;
	
	private int status;
	private String data;
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
