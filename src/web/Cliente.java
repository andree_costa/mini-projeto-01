package web;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {
	
	public Resposta request(String host, int port, String file) throws IOException, InterruptedException, ClassNotFoundException{
		
		Resposta resp;
		Socket socket;
		ObjectInputStream in;
		DataOutputStream out;
		
		try {
			socket = new Socket(host, port);
		} catch (UnknownHostException | ConnectException e) {
			System.out.println("Servidor n�o encontrado.");
			return null;
		}
		
		//System.out.println("Conectou ao servidor.");

		in = new ObjectInputStream(socket.getInputStream());
		out = new DataOutputStream(socket.getOutputStream());
		
		out.writeUTF(file);
		
		resp = (Resposta) in.readObject();
		
		socket.close();
		
		return resp;
	}
	
	public static Dados parseUri(String uri) {
		Dados dados;
		
		if (!uri.matches("^\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}:\\d{1,5}\\/\\S+$")) {
			System.out.println("URI inv�lida.");
			return null;
		}
		
		String[] list = uri.split("/");
		String host = list[0].split(":")[0];
		int port = Integer.parseInt(list[0].split(":")[1]);
		String file = list[1];
		
		if (port > 65535 || port < 1) {
			System.out.println("Porta inv�lida.");
			return null;
		}
			
		dados = new Dados(host, port, file);
		
		return dados;
	}

	public static void main(String[] args) throws ClassNotFoundException, IOException, InterruptedException {
		
		Cliente client = new Cliente();
		Scanner teclado = new Scanner(System.in);
		
		System.out.print("Infome a URI\n\n >");
		String uri = teclado.nextLine();
		teclado.close();
		
		Dados dados = parseUri(uri);
		if (dados == null) return;
		
		Resposta resp = client.request(dados.host, dados.port, dados.file);
		if (resp == null) return;
		
		if (resp.getStatus() == 200)
			System.out.println(resp.getData());
		else if (resp.getStatus() == 400)
			System.out.println("Arquivo n�o encontrado.");
	}
	
	public static class Dados {
		public String host;
		public int port;
		public String file;
		
		public Dados(String host, int port, String file) {
			this.host = host;
			this.port = port;
			this.file = file;
		}
	}
}
